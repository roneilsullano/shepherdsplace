import { ConfigService } from './services/config.service';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { encodeUriQuery } from '@angular/router/src/url_tree';
import { DatePickerComponent } from 'ngx-bootstrap';
import { DATE } from 'ngx-bootstrap/chronos/units/constants';

import { Router } from '@angular/router';



export interface LoginData 
  {
    "access_token": string,
    "refresh_token": string,
    "expires_in": number,
    "userName": string,
    "firstName": string,
    "lastName": string,
    "client_id": string,
    "isAdmin": boolean
}



@Injectable()
export class ServeService extends ConfigService {

  constructor(
    private http: HttpClient,
    private router: Router,
    
  ) {
    super();
   }

  isLogin(): boolean {
    let token = localStorage.getItem('token');
    if (token)
        return true;
    return false;
}


  getToken (data, endpoint) {
    console.log(this.api);
    // let api2 = 'http:/api.shepherdsplace.co.uk/api';
    let httpParams =  new HttpParams().append('client_id', data.client_id)
    .append('username', data.username)
    .append('password', data.password);
console.log(httpParams);
    return this.http.post<LoginData>(this.api + endpoint,  httpParams, this._loginHeader())
        .map(response => {
          console.log(response);
          var expiredin = Date.now() + (response['expires_in']*1000);

          console.log(Date.now());
          console.log(expiredin);
            
            localStorage.setItem('token', response['access_token']);
            localStorage.setItem('refresh_token', response['refresh_token']);
            localStorage.setItem('expired_in', expiredin.toString());
            return response;
        },
        error => {
        
          console.log(error);
            alert('invalid');
            return error;
        }
      );
  }
  
  getRefreshToken (endpoint) {
    console.log(this.api);
    // let api2 = 'http:/api.shepherdsplace.co.uk/api';
    let httpParams =  new HttpParams().append('refresh_token', localStorage.getItem('refresh_token'));
console.log(httpParams);
localStorage.clear();
    return this.http.post(this.api + endpoint,  httpParams, this._loginHeader())
        .map(response => {
          console.log(response);
          var expiredin: number = Date.now() + (response['expires_in']* 1000);
          localStorage.setItem('token', response['access_token']);
          localStorage.setItem('refresh_token', response['refresh_token']);
          localStorage.setItem('expired_in', expiredin.toString());
          return response;
        },
        error => {
            console.log(error);
            alert('invalid');
            this.router.navigate(['/login']);
            return error;
        }
      );
  }

  get( endpoint) {
    console.log(this.api);
    console.log(localStorage.getItem('token'));
    return this.http.get(this.api + endpoint,  this._tokenizedHeader())
        .map(response => {
          console.log(response);

            return response;
        },
        error => {
          console.log(error);
            alert('invalid');
            return error;
        }
      );
  }
  publicget( endpoint) {
    console.log(this.api);
    return this.http.get(this.api + endpoint,  this._publicHeader())
        .map(response => {
          console.log(response);

            return response;
        },
        error => {
          console.log(error);
            alert('invalid');
            return error;
        }
      );
  }

  publicPost(data , endpoint) {
    console.log(data);
    return this.http.post(this.api + endpoint, data, this._publicHeader())
    .map(response => {
      console.log(response);
      return response;
    },
    error => {
      console.log(error);
      alert('invalid');
    });

  }
post(data, endpoint) {
  console.log(data);
  return this.http.post(this.api + endpoint, data, this._tokenizedHeader())
  .map(response => {
    console.log(response);
    return response;
  },
  error => {
    alert('invalid');
  });

}
put(data, endpoint) {
  console.log(data);
  return this.http.put(this.api + endpoint, data, this._tokenizedHeader())
  .map(response => {
    console.log(response);
    return response;
  },
  error => {
    alert('invalid');
  });

}
delete(endpoint) {
  return this.http.delete(this.api + endpoint,  this._tokenizedHeader())
  .map(response => {
    console.log(response);
    return response;
  },
  error => {
    alert('invalid');
  }
  );
}
checkTokenExpiration() {
  const expired_in = parseInt(localStorage.getItem('expired_in'));
  const dateLogin = parseInt(localStorage.getItem('dateLogin'));
  if ( expired_in <= Date.now()  ) {
    this.getRefreshToken('/refresh')
    .subscribe(ret => {
      window.location.reload();
      console.log('Refresh token');
      console.log(ret);
    },
    error => {
      this.router.navigate(['/login']);
    });
  }
}
}
