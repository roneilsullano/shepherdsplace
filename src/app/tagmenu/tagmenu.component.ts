import { Component, OnInit } from '@angular/core';
import { ServeService } from './../serve.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-tagmenu',
  templateUrl: './tagmenu.component.html',
  styleUrls: ['./tagmenu.component.css']
})
export class TagmenuComponent implements OnInit {

  TagsData: any;
  pageNumber = 1;
  addTagForm: FormGroup;
  addTagSuccess = false;
  editStatus = false;
  clicktags = false;

  constructor(
      private serve: ServeService,
      private router: Router,
      private formBuilder: FormBuilder
    ) {
    this.getTags();
   }

  ngOnInit() {
    this.serve.checkTokenExpiration();
    this.addTagFormFunc();
  }

  addTagFormFunc() {
    this.addTagForm = this.formBuilder.group({
      name: [''],
      id:[null]
    });
  }
 hideEditForm(){
  this.editStatus = false;
 }
  showorhide() {
this.editStatus = !this.editStatus;

  }
  updateTag(id) {
     this.addTagForm.get('id').setValue(id);
    //  this.editForm.get('content').setValue(data.content);
    const data = JSON.parse(JSON.stringify(this.addTagForm.value));

    this.serve.put(data, '/Tag')
    .subscribe(ret => {
      Swal(
        'Success!',
        'Updated tag succesfully.',
        'success'
      )
      this.getTags();
    });
    
  }
  

  deletetag(id){
    Swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.serve.delete('/Tag/'+id)
    .subscribe(ret => {
    
      this.getTags();

    });
        Swal(
          'Deleted!',
          ' Tag has been deleted.',
          'success'
        )
     
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal(
          'Cancelled',
          'Your file is safe ',
          'error'
        )
      }
    })
    
  }
  getTags() {
    this.serve.get('/Tag')
    .subscribe(ret => {
      this.TagsData = ret;
      });
  }
  addTag(){
    const data = JSON.parse(JSON.stringify(this.addTagForm.value));
    this.serve.post(data, '/Tag')
    .subscribe(ret => {
      Swal(
        'Success!',
        'Submitted tag succesfully.',
        'success'
      )
     this.getTags();
     this.addTagForm.reset();
    });
    
  }
  Logout() {
    localStorage.clear();
     this.router.navigate(['/login']);
  }

}
 