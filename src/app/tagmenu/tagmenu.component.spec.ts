import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagmenuComponent } from './tagmenu.component';

describe('TagmenuComponent', () => {
  let component: TagmenuComponent;
  let fixture: ComponentFixture<TagmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
