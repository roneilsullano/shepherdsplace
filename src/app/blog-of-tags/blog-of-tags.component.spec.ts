import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogOfTagsComponent } from './blog-of-tags.component';

describe('BlogOfTagsComponent', () => {
  let component: BlogOfTagsComponent;
  let fixture: ComponentFixture<BlogOfTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogOfTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogOfTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
