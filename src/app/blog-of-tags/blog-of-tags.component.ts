import { state } from '@angular/animations';
import { Component, OnInit, Input, Directive } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ServeService } from './../serve.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-blog-of-tags',
  templateUrl: './blog-of-tags.component.html',
  styleUrls: ['./blog-of-tags.component.css']
})
export class BlogOfTagsComponent implements OnInit {
  testarray = [
	  
    {
      "id": "0",
      "name": "Professional Development ",
      "date": "September 11, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "7",
      "comment": "0",
      "img": "pd.png",
    
      "quote":"Here at Shepherds Place we take professional development very seriously.",
      "content":[
      " We recognise the need to continuously grow and get better at what we do. Becoming too comfortable would kill our business. Learning drives the engagement and productivity of our staff to ever greater heights. ",
      "We achieve this by collectively reflecting on our experience and pursuing official credentials as well. We constantly chat, often informally, throughout each of our projects about how to do things better. We sit down at the end of each project, data in hand, and make concrete plans about potential improvements for the next project. The aim is to make our process the most competent it possibly can be. The result shows in the quality of our work, the strength of our professional relationships, and the money we make. ",
      "A great example of this is our Site Manager, Rob, currently working towards becoming a qualified damp proofer. In several of our recent projects we have had trouble with unreliable damp proofers, and it always takes up a significant chunk of our budget. We decided to take matters into our own hands and Rob is now studying damp proofing. Like this we grow as a business and individuals. Once Rob is qualified the headache involved in damp proofing will be gone, we’ll save money for the business in the long term, and we have invested in the future of one of our own. This is a win all round. ",
      "Currently, there is not a single permanent member of our team that is not enrolled on a course that will help them and the business grow. This is the culture that allows us to be the most successful we can be. "
    ]
    },
    {
      "id": "1",
      "name": "Tyranny Of Numbers",
      "date": "August 29, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "300",
      "comment": "0",
      "img": "tyrannyofnumbers.jpg",
      "quote":"When looking for something to invest in there is a tendency to follow the numbers.",
      "content":[" We are driven by Return on Investment (ROI) or final profit. These stats will always be an important part of an investment but they can obscure the human element of the business. No matter what the numbers say we must recognise that we are always dealing with people and this is often more important than the stats.",
      "A good example of this is Houses in Multiple Occupancy (HMOs). The numbers on HMOs are always better than Buy to Lets (BTLs). The more tenants you can get in a place the greater the return on your investment. However, you get a different kind of person in a HMO than in a BTL. HMOs attract short-term tenants who may not look after the property and probably do not know each other. BTLs become people’s homes, families may live in them, and they stay for a long time. These people are much more stable tenants than you get in HMOs. ",
      "From the numbers we would see that HMOs give a much better financial return than BTLs. It is only when we consider the human element that BTLs become more attractive. HMOs, put simple, are more of a headache than BTLs. This is not always the case but they are always more likely to be a problem. This means more work for the landlord." ,
      "The same principle from this can be applied all throughout this business. You cannot just rely on the numbers when making a decision. You are tied to the people you are doing business with. This is a reality and must be respected when making business decisions. Here at Shepherds Place we always recognise the human element of our business. We seek to build long term relationships with everyone we do business with. For us, it’s not solely about the money. ",
    ]
    },
    
    {
      "id": "2",
      "name": "Work Culture",
      "date": "August 14, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "5",
      "comment": "0",
      "img": "workculture.jpg",
      "quote":"We expand our team when the work load demands it. ",
      "content":["Shepherds Place has recently expanded. With this addition, the in-house team has grown to four employees. We are also in constant contact with lawyers and our accountant. This brings the total up to 7. This comes with its challenges but is so essential to the running of the business. We simply needed more hands.  ",
      "Just because the work is there doesn’t mean the right person magically shows up. Because we have a small, that work from different locations it is essential we get the right kind of people. We look for people that are self-starters, incredibly trustworthy, take responsibility, and try their best. These are the kind of people Shepherds Place is built on.  ",
      "Take Rob, our on-site manager, for example. Rob has been with us since the start of Shepherds Place. After a property has been found, bought, the design work done, and all boxes ticked Rob’s work starts. Rob is so essential because he manages the work that really adds value to our properties. This company is built on his sweat and daily effort. ",
      "Rob has all the skills the role requires and, of course, there is the profit motive. These are the pre-requisites to having a business relationship. We look for a little extra. Rob is a great personality who is always looking to learn, improve, and make the business more efficient. ",
      "The best thing about our team is how we react when things go wrong. When something goes wrong we all say “good”. Nobody hides or blames. We see the opportunity for creativity and problem solving when something goes wrong. We’ve yet to come across a problem that was too big for us. "
    ]
    },	{
  
      "id": "3",
      "name": "What We Do",
      "date": "July 16, 2018",
      "author": "Christopher Chambers",
      "stars": "",
      "shares": "54",
      "comment": "0",
      "img": "whatwedo.jpg",
      "quote":"What do you do at Shepherds Place? ",
      "content":[" This is a question that we have been asked countless times. It needs a clear answer. Nobody should struggle to sum up what their business does. Over time Shepherds Place has expanded out from just property development to using our expertise to advise people with the capital and the desire to make their money work for them. ",
      "At the beginning, we would buy a house that we could add significant value to and improve it. This is still the basic principle we operate on. Shepherds Place is lucky enough to have two full time staff members that carry out our construction work. There are no outside contractors. Our team are trustworthy, reliable, and they deliver work to the highest standard. ",
      "On top of having an in-house construction team, we do the design work internally too. Our owner, Christopher Chamber, designs our refurbishments. He started Shepherds Place because of his love of design. He takes great pride in creating unique spaces and features that really add value to the properties. ",
      "When we put together investment packages, unlike other investment sourcers, we focus on properties that we can add a great deal of value to ourselves. Shepherds Place believes that you should earn your crust. We only feel comfortable when everyone is getting a great deal, a good principle for any business. ",
      "This approach makes Shepherds Place stand out amongst property developers and investment sourcers. Bringing these two things together allows for real value to be created while cutting out the cost of a middle man",
      "The results speak for themselves. We regularly make deals with yields around the 12% mark. This is what Shepherds Place can offer. Very few other companies can match us. "
      ]
    },
    {
      "id": "4",
      "name": "Professional Development ",
      "date": "September 11, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "7",
      "comment": "0",
      "img": "pd.jpg",
      "quote":"Here at Shepherds Place we take professional development very seriously.",
      "content":[
      " We recognise the need to continuously grow and get better at what we do. Becoming too comfortable would kill our business. Learning drives the engagement and productivity of our staff to ever greater heights. ",
      "We achieve this by collectively reflecting on our experience and pursuing official credentials as well. We constantly chat, often informally, throughout each of our projects about how to do things better. We sit down at the end of each project, data in hand, and make concrete plans about potential improvements for the next project. The aim is to make our process the most competent it possibly can be. The result shows in the quality of our work, the strength of our professional relationships, and the money we make. ",
      "A great example of this is our Site Manager, Rob, currently working towards becoming a qualified damp proofer. In several of our recent projects we have had trouble with unreliable damp proofers, and it always takes up a significant chunk of our budget. We decided to take matters into our own hands and Rob is now studying damp proofing. Like this we grow as a business and individuals. Once Rob is qualified the headache involved in damp proofing will be gone, we’ll save money for the business in the long term, and we have invested in the future of one of our own. This is a win all round. ",
      "Currently, there is not a single permanent member of our team that is not enrolled on a course that will help them and the business grow. This is the culture that allows us to be the most successful we can be. "
    ]
    },
    {
      "id": "5",
      "name": "Tyranny Of Numbers",
      "date": "August 29, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "300",
      "comment": "0",
      "img": "tyrannyofnumbers.jpg",
      "quote":"When looking for something to invest in there is a tendency to follow the numbers.",
      "content":[" We are driven by Return on Investment (ROI) or final profit. These stats will always be an important part of an investment but they can obscure the human element of the business. No matter what the numbers say we must recognise that we are always dealing with people and this is often more important than the stats.",
      "A good example of this is Houses in Multiple Occupancy (HMOs). The numbers on HMOs are always better than Buy to Lets (BTLs). The more tenants you can get in a place the greater the return on your investment. However, you get a different kind of person in a HMO than in a BTL. HMOs attract short-term tenants who may not look after the property and probably do not know each other. BTLs become people’s homes, families may live in them, and they stay for a long time. These people are much more stable tenants than you get in HMOs. ",
      "From the numbers we would see that HMOs give a much better financial return than BTLs. It is only when we consider the human element that BTLs become more attractive. HMOs, put simple, are more of a headache than BTLs. This is not always the case but they are always more likely to be a problem. This means more work for the landlord." ,
      "The same principle from this can be applied all throughout this business. You cannot just rely on the numbers when making a decision. You are tied to the people you are doing business with. This is a reality and must be respected when making business decisions. Here at Shepherds Place we always recognise the human element of our business. We seek to build long term relationships with everyone we do business with. For us, it’s not solely about the money. ",
    ]
    },
    
    {
      "id": "6",
      "name": "Work Culture",
      "date": "August 14, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "5",
      "comment": "0",
      "img": "workculture.jpg",
      "quote":"We expand our team when the work load demands it. ",
      "content":["Shepherds Place has recently expanded. With this addition, the in-house team has grown to four employees. We are also in constant contact with lawyers and our accountant. This brings the total up to 7. This comes with its challenges but is so essential to the running of the business. We simply needed more hands.  ",
      " Just because the work is there doesn’t mean the right person magically shows up. Because we have a small, that work from different locations it is essential we get the right kind of people. We look for people that are self-starters, incredibly trustworthy, take responsibility, and try their best. These are the kind of people Shepherds Place is built on.  ",
      "Take Rob, our on-site manager, for example. Rob has been with us since the start of Shepherds Place. After a property has been found, bought, the design work done, and all boxes ticked Rob’s work starts. Rob is so essential because he manages the work that really adds value to our properties. This company is built on his sweat and daily effort. ",
      "Rob has all the skills the role requires and, of course, there is the profit motive. These are the pre-requisites to having a business relationship. We look for a little extra. Rob is a great personality who is always looking to learn, improve, and make the business more efficient. ",
      "The best thing about our team is how we react when things go wrong. When something goes wrong we all say “good”. Nobody hides or blames. We see the opportunity for creativity and problem solving when something goes wrong. We’ve yet to come across a problem that was too big for us. "
    ]
    },	{
  
      "id": "7",
      "name": "What We Do",
      "date": "July 16, 2018",
      "author": "Christopher Chambers",
      "stars": "",
      "shares": "42",
      "comment": "0",
      "img": "whatwedo.jpg",
      "quote":"What do you do at Shepherds Place? ",
      "content":["What do you do at Shepherds Place? This is a question that we have been asked countless times. It needs a clear answer. Nobody should struggle to sum up what their business does. Over time Shepherds Place has expanded out from just property development to using our expertise to advise people with the capital and the desire to make their money work for them. ",
      "At the beginning, we would buy a house that we could add significant value to and improve it. This is still the basic principle we operate on. Shepherds Place is lucky enough to have two full time staff members that carry out our construction work. There are no outside contractors. Our team are trustworthy, reliable, and they deliver work to the highest standard. ",
      "On top of having an in-house construction team, we do the design work internally too. Our owner, Christopher Chamber, designs our refurbishments. He started Shepherds Place because of his love of design. He takes great pride in creating unique spaces and features that really add value to the properties. ",
      "When we put together investment packages, unlike other investment sourcers, we focus on properties that we can add a great deal of value to ourselves. Shepherds Place believes that you should earn your crust. We only feel comfortable when everyone is getting a great deal, a good principle for any business. ",
      "This approach makes Shepherds Place stand out amongst property developers and investment sourcers. Bringing these two things together allows for real value to be created while cutting out the cost of a middle man",
      "The results speak for themselves. We regularly make deals with yields around the 12% mark. This is what Shepherds Place can offer. Very few other companies can match us. "
      ]
    },{
      "id": "8",
      "name": "Professional Development ",
      "date": "September 11, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "1",
      "comment": "0",
      "img": "pd.jpg",
      "quote":"Here at Shepherds Place we take professional development very seriously.",
      "content":[
      " We recognise the need to continuously grow and get better at what we do. Becoming too comfortable would kill our business. Learning drives the engagement and productivity of our staff to ever greater heights. ",
      "We achieve this by collectively reflecting on our experience and pursuing official credentials as well. We constantly chat, often informally, throughout each of our projects about how to do things better. We sit down at the end of each project, data in hand, and make concrete plans about potential improvements for the next project. The aim is to make our process the most competent it possibly can be. The result shows in the quality of our work, the strength of our professional relationships, and the money we make. ",
      "A great example of this is our Site Manager, Rob, currently working towards becoming a qualified damp proofer. In several of our recent projects we have had trouble with unreliable damp proofers, and it always takes up a significant chunk of our budget. We decided to take matters into our own hands and Rob is now studying damp proofing. Like this we grow as a business and individuals. Once Rob is qualified the headache involved in damp proofing will be gone, we’ll save money for the business in the long term, and we have invested in the future of one of our own. This is a win all round. ",
      "Currently, there is not a single permanent member of our team that is not enrolled on a course that will help them and the business grow. This is the culture that allows us to be the most successful we can be. "
    ]
    },
    {
      "id": "9",
      "name": "Tyranny Of Numbers",
      "date": "August 29, 2018",
      "author": "Christopher Chambers",
      "stars": "5",
      "shares": "200",
      "comment": "0",
      "img": "tyrannyofnumbers.jpg",
      "quote":"When looking for something to invest in there is a tendency to follow the numbers. ",
      "content":["We are driven by Return on Investment (ROI) or final profit. These stats will always be an important part of an investment but they can obscure the human element of the business. No matter what the numbers say we must recognise that we are always dealing with people and this is often more important than the stats.",
      "A good example of this is Houses in Multiple Occupancy (HMOs). The numbers on HMOs are always better than Buy to Lets (BTLs). The more tenants you can get in a place the greater the return on your investment. However, you get a different kind of person in a HMO than in a BTL. HMOs attract short-term tenants who may not look after the property and probably do not know each other. BTLs become people’s homes, families may live in them, and they stay for a long time. These people are much more stable tenants than you get in HMOs. ",
      "From the numbers we would see that HMOs give a much better financial return than BTLs. It is only when we consider the human element that BTLs become more attractive. HMOs, put simple, are more of a headache than BTLs. This is not always the case but they are always more likely to be a problem. This means more work for the landlord." ,
      "The same principle from this can be applied all throughout this business. You cannot just rely on the numbers when making a decision. You are tied to the people you are doing business with. This is a reality and must be respected when making business decisions. Here at Shepherds Place we always recognise the human element of our business. We seek to build long term relationships with everyone we do business with. For us, it’s not solely about the money. ",
    ]
    }
  ]; 
  pageBlogNumber = 1;
blogData: any = [];
showblog: boolean = false;
featuredblog: any = {};
blogSearch: string;
default: string = '/assets/img/default.png';
apilink: string = 'https://api.shepherdsplace.co.uk/api/public/file/';
statustag: any = this.ActivatedRoute.snapshot.params.tag;
  constructor( private ActivatedRoute: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
    private serve: ServeService,
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient) { }

  ngOnInit() {
    this.ngxService.start();
    this.getblogs();
  }
  submit() {
    this.router.navigate(['/bloglist-of-tag/' + this.blogSearch]);

}
  viewBlog(data) {
    this.router.navigate(['/single-blog', data.id , data.headerUrl]);
  }
  getblogs() {
     console.log('here');
    this.serve.publicget('/public/blog/tag/' + this.statustag)
    .subscribe(ret => {
      this.blogData = ret;
      console.log(this.blogData);
      this.showblog = true;
      this.ngxService.stop();
    },
  error => {
    console.log(error);
  }
  );
}



}
