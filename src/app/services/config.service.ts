import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class ConfigService {
  api: any = 'https://api.shepherdsplace.co.uk/api';
  blogdata: any = {'status': 'normal', 'id': null, 'buttonType': 'danger', 'buttonMessage': 'Delete', 'data': null};
  nginitStatus = true;
  fileData: any;
  constructor() { }

  _publicHeader = (headers?: HttpHeaders | null): object => {
    headers = headers || new HttpHeaders();
    headers = headers.set('Accept', 'application/json, charset=utf-8');
    return {
      headers: headers
    };
  }

  _loginHeader = (headers?: HttpHeaders | null): object => {
    headers = headers || new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    return {
      headers: headers
    };
  }

  _tokenizedHeader = (headers?: HttpHeaders | null): object => {
    headers = headers || new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    headers = headers.set('Accept', 'application/json, charset=utf-8');
    return {
      headers: headers
    };
  }
  _setFileforBlog(status, id, type, message, data) {
    this.blogdata = {'status': status, 'id': id, 'buttonType': type, 'buttonMessage': message, 'data': data};
  }

  _GetFileforBlog() {
    return this.blogdata;
  }

  _setRunnginit(flag) {
    this.nginitStatus = flag;
  }
  _GetRunnginit() {
    return this.nginitStatus;
  }
  _setFileData(data) {
this.fileData = data;
  }
  _getFileData() {
return this.fileData;
  }

}
