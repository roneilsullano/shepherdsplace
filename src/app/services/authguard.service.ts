import { ServeService } from './../serve.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';


@Injectable()
export class AuthGuardService implements CanActivate{
    constructor(
        protected _router: Router, 
        protected _serveService: ServeService
    ) {}

    canActivate() {
        if(!localStorage.getItem('token')){
            console.log("welcome");
            this._router.navigate(['/login'])
        }
        
     
        if (this._serveService.isLogin()){
        return true;
        }
       
    }



    
}
