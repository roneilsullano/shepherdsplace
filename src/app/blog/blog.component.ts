import { state } from '@angular/animations';
import { Component, OnInit, Input, Directive } from '@angular/core';
import { Router } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ServeService } from './../serve.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';

import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  pageBlogNumber = 1;
  blogData: any = [];
  showblog = false;
  blogSearch: string;
  featuredblog: any = {};
  default = '/assets/img/default.png';
  apilink = 'https://api.shepherdsplace.co.uk/api/public/file/';
  constructor(
    private ngxService: NgxUiLoaderService,
    private serve: ServeService,
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient) { }

  ngOnInit() {
    this.ngxService.start();
    this.getblogs();
  }
  submit() {
     this.router.navigate(['/bloglist-of-tag/' + this.blogSearch]);
}
  viewBlog(data) {
    this.router.navigate(['/single-blog', data.id, data.headerUrl]);
  }
  getblogs() {
    this.serve.publicget('/public/blog/preview')
    .subscribe(ret => {
      this.blogData = ret;
      this.featuredblog = this.blogData[0];
       this.blogData.splice(0, 1);
      console.log( this.featuredblog);
      console.log(this.blogData);
      this.showblog = true;
      this.ngxService.stop();
    },
  error => {
    console.log(error);
  }
  );
}


}
