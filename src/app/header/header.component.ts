import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { Globals } from '../globals';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
   private status: boolean[];
  
  
  constructor(router: Router, private globals: Globals)
   {  this.status = globals.status;
  
    }
    public open(event, item) {
      for (let index = 0; index < 3; index++) {
        this.globals.status [index]=false;
      }
      this.globals.status [item]=true;
      console.log(this.globals.status);
    }
    public home() {
      

    }

  
 


  ngOnInit() {

  }

}
