import Swal from 'sweetalert2';
import { state } from '@angular/animations';
import { Component, OnInit, Input, Directive } from '@angular/core';
import { Router } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { ServeService } from './../serve.service';
import{FormBuilder, FormGroup, Validators} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { disableDebugTools } from '@angular/platform-browser';

@Component({
  selector: 'app-blog-admin',
  templateUrl: './blog-admin.component.html',
  styleUrls: ['./blog-admin.component.css']
})


export class BlogAdminComponent implements OnInit {
  editForm: FormGroup;
  // addForm: FormGroup;
  status:boolean = false;
 
  editstatus: boolean = false;
  showstatus:boolean = false;
  blogData:any = {};
  
  pageTagsNumber = 1;
  pageBlogNumber = 1;
  showbloglist:boolean = false;

  

  constructor(
    private serve: ServeService,
    private router: Router,
    private fb:FormBuilder, 
    private http: HttpClient) { 
    this.editForm = fb.group({
      'metaDescription':[""],
      'header':[""],
     
      'author':[""],
      'shortContent':[""],
      'publishDate':[""],
      'blogEntryComments':[""],
      'blogEntryTags':[""],
      'visits':[""],
      'content':[""],
    });
  }
 

  ngOnInit() {

    this.getblogs();
    this.getTags();
    this.serve.checkTokenExpiration();
  }
  
 
 

  getblogs(){
     
      this.serve.get('/blog')
      .subscribe(ret => {
        this.blogData = ret;
        this.status = true;
  
      },
    error => {
      console.log(error);
    }
    );
  }

   
  deleteBlog(id){
    Swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.serve.delete('/blog/'+id)
        .subscribe(ret => {
        
          this.getblogs();
    
        });
        Swal(
          'Deleted!',
          'Blog has been deleted.',
          'success'
        )
     
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal(
          'Cancelled',
          'Blog is safe ',
          'error'
        )
      }
    })
  
  }

  viewBlog(data){
    this.router.navigate(['/single-blog',data.id,data.headerUrl])
  }
  editBlog(id){
    this.router.navigate(['/edit-blog/', id]);
  }
  addBlog(){
    this.router.navigate(['/add-blog']);
  }







  // tags function
  currentblogid:any="";
  blogTags: any = {'tagIds':[]}; 
  tagsIndex:any= [];
  statusTags:boolean = false;
  tagsData:any={};

  removeSingleTag(data, id){
    console.log(data);
    console.log(id);
    this.blogTags.tagIds=[];
    for (let entry of data.tags) {
      if (id!=entry.id) {
        this.blogTags.tagIds.push(entry.id);
      }
  }

    const blog = JSON.parse(JSON.stringify( this.blogTags));
    this.serve.put(blog, '/Blog/'+data.id+'/tag')
    .subscribe(ret => {
      this.getblogs();
      
    });
      }
  currentblog(blog){
    this.blogTags.tagIds=[];
     for (let index = 0; index < this.tagsData.length; index++) {
      this.tagsData[index].status= false;
       
     }
    this.currentblogid = blog.id;
    for (let index = 0; index < blog.tags.length; index++) {
      for (let index1 = 0; index1 < this.tagsData.length; index1++) {
        if(blog.tags[index].id == this.tagsData[index1].id){
          this.tagsData[index1].status = true;
          this.blogTags.tagIds.push(this.tagsData[index1].id);
         
        }
        
      }      
    }
    console.log( this.blogTags.tagIds);
    console.log(this.tagsData)
    this.statusTags = true;
  } 

  getTags(){
    
    this.serve.get('/Tag')
    .subscribe(ret => {
      this.tagsData = ret;


      });
  }
  submitTags(){
    
    const data = JSON.parse(JSON.stringify( this.blogTags));
    this.serve.put(data, '/Blog/'+this.currentblogid+'/tag')
    .subscribe(ret => {
      Swal(
        'Success!',
        'Tag/s has been added.',
        'success'
      )
      this.getblogs();
      
    });
    this.showstatus = false;

  }
  
 
  addTag(tag, index){
    this.blogTags.tagIds.push(tag.id);
    this.tagsData[index].status = true;
    console.log(this.blogTags);
  } 
  removeTag(tag, ind){
    for (let index = 0; index < this.blogTags.tagIds.length; index++) {
      if (this.blogTags.tagIds[index]==tag.id) {
        this.blogTags.tagIds.splice(index, 1);
      }
      
    }
    
     this.tagsData[ind].status = false;
    console.log(this.blogTags);
  }
  // logout
  Logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
