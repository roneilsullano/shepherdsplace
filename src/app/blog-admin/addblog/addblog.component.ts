import  Swal  from 'sweetalert2';
import { state } from '@angular/animations';
import { Component, OnInit, Input, Directive } from '@angular/core';
import { Router } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { ServeService } from './../../serve.service';
import{FormBuilder, FormGroup, Validators, MaxLengthValidator} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { disableDebugTools } from '@angular/platform-browser';

declare var $ :any;

@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.css']
})
export class AddblogComponent implements OnInit {

  addForm: FormGroup;
  status:boolean = false;
  fileData:any;
  blogData:any = {};
  default:string="/assets/img/default.png";
  public options: Object = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: true,
    height: 300,
    imageInsertButtons: ['imageBack', '|', 'imageByURL','imageManager'],
    imagePaste: false,
    imageManagerLoadURL: "https://api.shepherdsplace.co.uk/api/public/file"

  }
  date: Date = new Date();
  settings = {
      bigBanner: false,
      timePicker: false,
      format: 'dd-MM-yyyy',
      defaultOpen: false
  }
  

  constructor(
    private serve: ServeService,
    private router: Router,
    private fb:FormBuilder, 
    private http: HttpClient) { 
    this.addForm = fb.group({
      'metaDescription':[""],
      'header':[""],
      'headerUrl':[""],
      'author':[""],
      'shortContent':["", Validators.maxLength(100)],
      'publishDate':[this.date],
      'content':[""],
    });
  }

  ngOnInit() {
    if(this.serve._GetRunnginit()){
      console.log(this.serve._GetRunnginit());
      this.status=true;
    }
    else{
      this.blogData=this.serve._GetFileforBlog();
      this.blogData= this.blogData.data;
      this.fileData= this.serve._getFileData();
      console.log(this.fileData);
      console.log(this.blogData);
      this.default = "https://api.shepherdsplace.co.uk/api/public/file/"+ this.fileData; 
      this.enableForms(this.blogData);
    }
    
  }
  
  enableForms(data){
    console.log(this.status);

    this.addForm.get('headerUrl').setValue(data.header);
      this.addForm.get('header').setValue(data.header);
      this.addForm.get('metaDescription').setValue(data.metaDescription);
      this.addForm.get('author').setValue(data.author);
      this.addForm.get('shortContent').setValue(data.shortContent);
      this.addForm.get('publishDate').setValue(data.publishDate);
      this.addForm.get('content').setValue(data.content);
      this.status=true;
      
  }
  addBlog(){
    this.addForm.get('publishDate').setValue(this.date);
    var data2={"fileIds": [this.fileData ]  };
    data2 =JSON.parse(JSON.stringify(data2));
    const data = JSON.parse(JSON.stringify(this.addForm.value));
    this.serve.post(data, '/Blog')
    .subscribe(ret => {
      this.blogData = ret; 
    this.addForm.reset();
    this.serve.put(data2, '/Blog/'+this.blogData.id+'/File')
    .subscribe(ret => {
      Swal(
        'Success!',
        'Blog has been Added.',
        'success'
      )
    this.serve._setRunnginit(true);
      this.router.navigate(['/blogAdmin']);
    });
    });
    
  }

  addChangeFile(){
 
    this.serve._setRunnginit(false);
    const data = JSON.parse(JSON.stringify(this.addForm.value));
    this.serve._setFileforBlog("add", null, 'primary', 'Submit', data);

    this.router.navigate(['/media' ]);
  }
}
