import  Swal  from 'sweetalert2';
import { Component, OnInit, Input, Directive } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { ServeService } from './../../serve.service';
import{FormBuilder, FormGroup, Validators} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { disableDebugTools } from '@angular/platform-browser';
import { DateFormatter } from 'ngx-bootstrap';

@Component({
  selector: 'app-editblog',
  templateUrl: './editblog.component.html',
  styleUrls: ['./editblog.component.css']
})
export class EditblogComponent implements OnInit {
  date: Date = new Date();
  settings = {
      bigBanner: false,
      timePicker: false,
      format: 'dd-MM-yyyy',
      defaultOpen: false
  }
  editForm: FormGroup;
  // addForm: FormGroup;
  status:boolean = false;
  editstatus: boolean = false;
  blogData:any = {};
  public options: Object = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: true,
    height: 300,
    imageInsertButtons: ['imageBack', '|', 'imageByURL','imageManager'],
    imagePaste: false,
    imageManagerLoadURL: "https://api.shepherdsplace.co.uk/api/public/file"

  }

  default:string="/assets/img/default.png";
  fileData: any;
  constructor(
    private ActivatedRoute: ActivatedRoute,
    private serve: ServeService,
    private router: Router,
    private fb:FormBuilder, 
    private http: HttpClient) { 
    this.editForm = fb.group({
      'metaDescription':[""],
      'header':[""],
      'headerUrl':[""],
      'author':[""],
      'shortContent':["", Validators.maxLength(100)],
      'publishDate':[""],
    'id':[""],
      'content':[""],
    });
  }
  statusid:any=this.ActivatedRoute.snapshot.params.id;
 
  ngOnInit() {
    this.serve.checkTokenExpiration();
  if(this.serve._GetRunnginit()){
    console.log(this.serve._GetRunnginit());
    this.getblogs();

  }
  else{
    this.blogData=this.serve._GetFileforBlog();
    this.blogData= this.blogData.data;
    this.fileData= this.serve._getFileData();
    console.log(this.fileData);
    this.default = "https://api.shepherdsplace.co.uk/api/public/file/"+ this.fileData; 
    this.enableForms(this.blogData);

  }
  }
  enableForms(data){
    this.editstatus= true;
    console.log(this.editstatus);

   
      this.editForm.get('header').setValue(data.header);
      this.editForm.get('metaDescription').setValue(data.metaDescription);
      this.editForm.get('headerUrl').setValue(data.headerUrl);
      this.editForm.get('author').setValue(data.author);
      this.editForm.get('shortContent').setValue(data.shortContent);
      this.editForm.get('publishDate').setValue(data.publishDate);
      this.editForm.get('content').setValue(data.content);
      this.editForm.get('id').setValue(data.id);
      
      this.status = true;

  }
  editBlog(){
   var data2={"fileIds": [this.fileData ]  };
   data2 =JSON.parse(JSON.stringify(data2));
    const data = JSON.parse(JSON.stringify(this.editForm.value));
    this.serve.put(data, '/blog')
    .subscribe(ret => {
      this.serve.put(data2, '/Blog/'+data.id+'/File')
      .subscribe(ret => {
        Swal(
          'Success!',
          'Blog has been updated.',
          'success'
        )
      this.serve._setRunnginit(true);
        this.router.navigate(['/blogAdmin', ]);
       
      });
    
    });
  }
  addChangeFile(){
 
    this.serve._setRunnginit(false);
    const data = JSON.parse(JSON.stringify(this.editForm.value));
    this.serve._setFileforBlog("edit", this.blogData.id, 'primary', 'Submit', data);
    this.router.navigate(['/media' ]);
  }
  getblogs(){
    this.serve.get('/Blog/'+this.statusid)
    .subscribe(ret => {
      this.blogData = ret;
if(this.blogData.files.length>0){
  this.default="https://api.shepherdsplace.co.uk/api/public/file/"+this.blogData.files[0].id;
  this.fileData=this.blogData.files[0].id;
}

      this.enableForms(this.blogData);
      
    },
  error => {
    console.log(error);
  }
  );
  
}
}
