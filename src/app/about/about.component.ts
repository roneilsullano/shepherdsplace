import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})

export class AboutComponent implements OnInit {
  public status: boolean[];

  constructor(private globals: Globals) {
    
this.status = globals.status;
  }

  public open(event, item) {
    for (let index = 0; index < 3; index++) {
      this.globals.status [index]=false;
    }
    this.globals.status [item]=true;
    console.log(this.globals.status);
  }
  public home() {
    

  }

  ngOnInit() {
  }

}
