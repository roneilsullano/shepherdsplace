
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { StickyNavModule } from 'ng2-sticky-nav';
import {CommonModule} from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapsibleModule } from 'angular2-collapsible';
import { Globals } from './globals';
import { FragmentPolyfillModule } from './fragment-polyfill.module';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ServeService } from './serve.service';
import { ConfigService } from './services/config.service';
import { AuthGuardService } from './services/authguard.service';
import { SidebarModule } from 'ng-sidebar';
import { CeiboShare } from 'ng2-social-share';
import {NgxPaginationModule} from 'ngx-pagination';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import Swal from 'sweetalert2';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';



import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BlogAdminComponent } from './blog-admin/blog-admin.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PropertyDevelopmentComponent } from './property-development/property-development.component';
import { InvestmentSourcingComponent } from './investment-sourcing/investment-sourcing.component';
import { YorkshireOrientationComponent } from './yorkshire-orientation/yorkshire-orientation.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { SingleComponent } from './single/single.component';
import { LoginComponent } from './login/login.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { TagmenuComponent } from './tagmenu/tagmenu.component';
import { FilemenuComponent } from './filemenu/filemenu.component';
import { EditblogComponent } from './blog-admin/editblog/editblog.component';
import { AddblogComponent } from './blog-admin/addblog/addblog.component';
import { BlogOfTagsComponent } from './blog-of-tags/blog-of-tags.component';
import { CommentsComponent } from './comments/comments.component';

const appRoutes: Routes = [
{path: '', component: HomeComponent},
{path: 'blog', component: BlogComponent},
{path: 'login', component: LoginComponent},
{path: 'about', component: AboutComponent},
{path: 'single-blog/:id/:headerurl', component: SingleComponent},
{path: 'blog-admin', component: BlogAdminComponent,  canActivate: [AuthGuardService]},
{path: 'bloglist-of-tag/:tag', component: BlogOfTagsComponent},
{path: 'add-blog', component: AddblogComponent,  canActivate: [AuthGuardService]},
{path: 'edit-blog/:id', component: EditblogComponent,  canActivate: [AuthGuardService]},
{path: 'blog-admin', component: BlogAdminComponent,  canActivate: [AuthGuardService]},
{path: 'media', component: FilemenuComponent,  canActivate: [AuthGuardService]},
{path: 'tag-menu', component: TagmenuComponent},
{path: 'comment-menu', component: CommentsComponent},

{path: '', redirectTo: '/login', pathMatch: 'full'},
{path: '**', redirectTo: '/login', pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    SidebarComponent,
    BlogAdminComponent,

    CeiboShare,
    PropertyDevelopmentComponent,
    InvestmentSourcingComponent,
    YorkshireOrientationComponent,
    BlogComponent,
    ContactComponent,
    SingleComponent,
    LoginComponent,
    SidemenuComponent,
    TagmenuComponent,
    FilemenuComponent,
    EditblogComponent,
    AddblogComponent,
    BlogOfTagsComponent,
    CommentsComponent
  ],
  imports: [
    NgxPaginationModule,
    TooltipModule.forRoot(),
    NgxEditorModule ,
    AngularDateTimePickerModule,
    BrowserModule,
    AngularFontAwesomeModule,
    HttpModule,
    BrowserAnimationsModule,
    SidebarModule.forRoot(),
    BrowserAnimationsModule,
    NgxUiLoaderModule,
    HttpClientModule,
    StickyNavModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    FragmentPolyfillModule.forRoot({smooth: true}),
    RouterModule.forRoot(appRoutes) ,
    CollapsibleModule ,
    BrowserAnimationsModule,

  ],
  providers: [ServeService, AuthGuardService, ConfigService, Globals],
  bootstrap: [AppComponent]

})
export class AppModule { }
