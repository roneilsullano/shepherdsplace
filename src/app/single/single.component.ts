import { Component, OnInit, Input, Directive, Injectable } from '@angular/core';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { ServeService } from './../serve.service';
import{FormBuilder, FormGroup, Validators} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';

import { ActivatedRoute, Router  } from '@angular/router';
import { CeiboShare } from 'ng2-social-share';

import { fadeInAnimation } from "./../animations/fadeIn.animation";
import { slideInFadeInAnimation } from "./../animations/slideInFadeIn.animation";
import { trigger, state, animate, transition, style } from "@angular/animations";
import "rxjs/add/operator/catch";
import "rxjs/Rx";
import 'rxjs/add/operator/map';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';

import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css'],
  animations: [
    trigger(
        "moveLabel", [
            state("moveUp", style({
                opacity: 0.5,
                top: "4px",
                fontSize: "14px"
            })),
            state("moveDown", style({
                opacity: 0.2,
                top: "20px",
                fontSize: "14px"

            })),
            transition("* => moveUp", animate("400ms ease-in-out")),
            transition("* => moveDown", animate("400ms ease-in-out"))
        ]),
    fadeInAnimation,
    slideInFadeInAnimation
]
  
})
@Directive({
selector: '[CeiboShare]'
  })
export class SingleComponent implements OnInit {

public repoUrl = '' ;
public imageUrl = '' ;
state = 'moveDown';
nameState = 'moveDown';
emailState = 'moveDown';
commentState = 'moveDown';
formState = "active"; 

rForm: FormGroup;
post: any;
formdata: any;
Name: string;
Email: string;
Comment: string;
blogData: any;
blogsData: any;
showblog = false;
apilink = 'https://api.shepherdsplace.co.uk/api/public/file/';
pageBlogNumber = 1;
comments: any = [];
log: any = {};

constructor(private ActivatedRoute: ActivatedRoute,
private ngxService: NgxUiLoaderService,
private router: Router,
private fb: FormBuilder,
private http: HttpClient,
private serve: ServeService,
) {

this.rForm = fb.group({
'Name': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern('[^0-9]*')])],
'Email': ['', Validators.compose([Validators.required, Validators.email])],
'Comment': ['', Validators.compose([Validators.required, Validators.minLength(1)])]
});
  }
  statusid: any = this.ActivatedRoute.snapshot.params.id;
  Logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('refresh_token');
    this.router.navigate(['/blog']);
  }

  inputFocusIn(event: any) {
     
    switch (event.target.id) {
        case "Name":
            this.nameState = "moveUp";
            break;
        case "Email":
            this.emailState = "moveUp";
            break;
      
        case "Comment":
            this.commentState = "moveUp";
            break;
    }
}
inputFocusOut(event: any) {

  if (this.rForm.get(event.target.id)!.value == null ||
      this.rForm.get(event.target.id)!.value != "") {
      return;
  }

  switch (event.target.id) {
      case "Name":
          this.nameState = "moveDown";
          break;
      case "mail":
          this.emailState = "moveDown";
          break;
      
        case "Comment":
          this.commentState = "moveDown";
          break;
  }


}

  ngOnInit() {

this.ngxService.start();
console.log(this.router.url);
console.log(this.statusid);
this.getblog(this.statusid);
this.repoUrl = 'https://dev.shepherdsplace.co.uk' + this.router.url;
this.visitCount();
  }
  visitCount() {
    this.log = this.statusid;
    this.serve.publicPost(this.log, '/public/blog/visit/' + this.statusid)
    .subscribe(ret => {
    });
  }
  shareCount() {
    this.log = this.statusid;
    this.serve.publicPost('' , '/public/blog/share/' + this.statusid)
    .subscribe(ret => {
    console.log(ret);
    });
  }
  getComments() {
    console.log('here');
    this.serve.publicget('/public/blog/comment/' + this.statusid)
    .subscribe(ret => {
    this.comments = ret;
    },
  error => {
    console.log(error);
  }
  );
  }
  getblog(id) {
   this.serve.publicget('/public/blog/' + id)
   .subscribe(ret => {
        this.blogData = ret;
        this.showblog = true;
this.imageUrl = this.apilink + this.blogData.blogEntryFiles[0].file.id;
this.ngxService.stop();
this.getblogs();
this.getComments();
   },
 error => {
   console.log(error);
 }
 );

}
getblogs() {

   this.serve.publicget('/public/blog')
   .subscribe(ret => {
this.blogsData = ret;

console.log(this.blogsData);

   },
 error => {
   console.log(error);
 }
 );
}


selectblog(data) {


location.replace('single-blog/' + data.id + '/' + data.headerUrl);
}

submitcomment(data) {
    data.blogEntryId = this.statusid;
    const commentData = JSON.parse(JSON.stringify(data));
    this.serve.publicPost(commentData, '/public/blog/comment')
    .subscribe(ret => {
        this.getComments();
    });
    }
}
export declare class FacebookParams {
    u: string;
}
export class GooglePlusParams {
    url: string;
}
export class LinkedinParams {
    url: string;
}
export declare class PinterestParams {
    url: string;
    media: string;
    description: string;
}
export class TwitterParams {
    text: string;
    url: string;
    hashtags: string;
    via: string;
}
