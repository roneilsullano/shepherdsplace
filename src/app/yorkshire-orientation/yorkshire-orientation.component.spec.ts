import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YorkshireOrientationComponent } from './yorkshire-orientation.component';

describe('YorkshireOrientationComponent', () => {
  let component: YorkshireOrientationComponent;
  let fixture: ComponentFixture<YorkshireOrientationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YorkshireOrientationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YorkshireOrientationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
