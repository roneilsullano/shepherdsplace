import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// @NgModule({
//   imports: [],
// })
@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})

export class SidemenuComponent implements OnInit {

  config = {
    panels: [
      { name: 'Section 1', description: 'First section' },
      { name: 'Section 2', description: 'Second section' },
      { name: 'Section 3', description: 'Third section' }
    ]
  };
  constructor( private router: Router) {
    
}

 
addBlog(){
  this.router.navigate(['/add-blog']);
}


  ngOnInit() {
  }

}
