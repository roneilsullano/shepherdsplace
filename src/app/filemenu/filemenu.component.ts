import { Component, OnInit } from '@angular/core';
import { ServeService } from './../serve.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-filemenu',
  templateUrl: './filemenu.component.html',
  styleUrls: ['./filemenu.component.css']
})

export class FilemenuComponent implements OnInit {
  apilink: string = 'https://api.shepherdsplace.co.uk/api';
  title = 'app works!';
  fileResult: any;
  filesData: any;
  base64textString: any;
  pageNumber = 1;
  addFileForm: FormGroup;
  addTagSuccess = false;
  editStatus:boolean = false;
  clicktags: boolean = false;3
  status:boolean = false;
  fileData: any;
  ext:any="";


  selectedImage:any={'id':null};
  selectedImageStatus:boolean = false;
  processData:any;

  constructor(
      private serve: ServeService,
      private router: Router,
      private formBuilder: FormBuilder
    ) {
    this.getFile();
   }

  ngOnInit() {
    this.serve.checkTokenExpiration();
    this.addFileFormFunc();
    this.processData = this.serve._GetFileforBlog();
    console.log(this.processData);
  }

    

  addFileFormFunc() {
    this.addFileForm = this.formBuilder.group({
      byteArray: ['',Validators.required],
      extension: [''],
      fileData: ['']
    });
  }
 
  

  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];
    this.ext = file.name.split('.').pop();
    // this.fileData.extension = ext;
    this.addFileForm.get('extension').setValue(this.ext);
    console.log(this.ext);
  if (files && file) {
      var reader = new FileReader();
      reader.onload =this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    
      
      
  }
}

_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          this.addFileForm.get('byteArray').setValue(btoa(binaryString));
         
  }

  selectImage(data){
    this.selectedImageStatus = true;
    this.selectedImage = data;
console.log(data);
  }
  

  ProcessFile(){
    if(this.processData.status=="normal"){
      Swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this file!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {
          this.serve.delete('/File/'+this.selectedImage.id)
          .subscribe(ret => {
            this.getFile();
          });
          Swal(
            'Deleted!',
            'File has been deleted.',
            'success'
          )
       
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal(
            'Cancelled',
            'file is safe ',
            'error'
          )
        }
      })
    
  }
  else if(this.processData.status=="edit"){
    this.serve._setFileData(this.selectedImage.id);
      this.router.navigate(['/edit-blog/'+ this.processData.data]);
  }
  else if(this.processData.status=="add"){
    this.serve._setFileData(this.selectedImage.id);
      this.router.navigate(['/add-blog/']);
  }
  this.selectedImageStatus = false;
  }
addFile(){
    this.addFileForm.removeControl('fileData');
    const data = JSON.parse(JSON.stringify(this.addFileForm.value));
    this.serve.post(data, '/File')
    .subscribe(ret => {
      Swal(
        'Success!',
        'Updated File succesfully.',
        'success'
      )
      this.selectedImageStatus = false;
     this.getFile();
    });
    
   }
  getFile() {
    this.selectedImage={'id':null};
    this.serve.get('/file/')
    .subscribe(ret => {
      this.filesData = ret;
      this.status= true;
      });
  }
  
  Logout() {
    localStorage.clear();
     this.router.navigate(['/login']);
  }

}
