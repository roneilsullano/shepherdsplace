import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilemenuComponent } from './filemenu.component';

describe('FilemenuComponent', () => {
  let component: FilemenuComponent;
  let fixture: ComponentFixture<FilemenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilemenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilemenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
