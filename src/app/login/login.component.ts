import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServeService } from './../serve.service';
import{FormBuilder, FormGroup, Validators} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpModule} from '@angular/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  rForm: FormGroup;

  formdata:any;
  client_id:string="";
  username:string="";
  password:string="";
  status: any="";
  intdate: number;



  data: {'client_id': string, 'username': string, 'password': string};
  constructor(  private ngxService: NgxUiLoaderService, private serve: ServeService, private router: Router, private fb:FormBuilder,  private http: HttpClient) {
    this.rForm = fb.group({
      'client_id':[""],
      'username':[""],
      'password':[""],
     
    })

    this.data = {client_id : 'ShepherdsPlace', username : '', password : ''};
   }

  ngOnInit() {
    if (localStorage.getItem('token')!=null) {
      
    
      if (Date.now() >  parseInt(localStorage.getItem('expired_in'))) {
        console.log(Date.now());
        this.router.navigate(['/blog-admin']);
      }
      else if(Date.now() <  parseInt(localStorage.getItem('expired_in'))){
        console.log("expired");
        localStorage.clear();
         this.router.navigate(['/login']);
      }
    }
       
  }
  onSubmit(post) {
    this.ngxService.start();
    this.data.username = post.username;
    this.data.password = post.password;
    console.log(this.data);
    this.serve.getToken(this.data, '/token')
    .subscribe(ret => {
      console.log(ret);
      this.ngxService.stop();
      this.router.navigate(['blog-admin']);
    },
  error => {
    this.ngxService.stop();
    console.log(error);
    this.status = error;
  }
  );
  }
}
