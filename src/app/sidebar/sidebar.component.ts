

import { HttpClientModule } from '@angular/common/http'; 
import { Component, OnInit , Injectable} from '@angular/core';
import{FormBuilder, FormGroup, Validators} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpModule} from '@angular/http';
import 'rxjs/add/operator/map';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { fadeInAnimation } from "./../animations/fadeIn.animation";
import { slideInFadeInAnimation } from "./../animations/slideInFadeIn.animation";
import { trigger, state, animate, transition, style } from "@angular/animations";
import "rxjs/add/operator/catch";
import "rxjs/Rx";

export interface LoginData 
  {
    'name': any,
    'email':any,
    'phone':any,
    'postcode':any,
    'subject':any,
    'message':any
  
  }
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  animations: [
    trigger(
        "moveLabel", [
            state("moveUp", style({
                opacity: 0.5,
                top: "4px",
                fontSize: "14px"
            })),
            state("moveDown", style({
                opacity: 0.2,
                top: "20px",
                fontSize: "14px"

            })),
            transition("* => moveUp", animate("400ms ease-in-out")),
            transition("* => moveDown", animate("400ms ease-in-out"))
        ]),
    fadeInAnimation,
    slideInFadeInAnimation
]
})
export class SidebarComponent implements OnInit {
  rForm: FormGroup;
  post: any;
  formdata:any;
  name:string="";
  email:string="";
  phone:string="";
  message:string="";

  state: string = "moveDown";
  nameState: string = "moveDown";
  emailState: string = "moveDown";
  phoneState: string = "moveDown";
  messageState: string = "moveDown";
  formState: string = "active"; 

  constructor(private fb:FormBuilder,  private http: HttpClient){ 
    this.rForm = fb.group({
      'name':["", Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern("[^0-9]*")])],
      'email':["", Validators.compose([Validators.required, Validators.email])],
      'phone':["", Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern("[0-9]*")])],
      'message':[null],
    })

    
    
  }
  inputFocusIn(event :any) {
     
    switch (event.target.id) {
        case "name":
            this.nameState = "moveUp";
            break;
        case "email":
            this.emailState = "moveUp";
            break;
        case "phone":
            this.phoneState = "moveUp";
            break;
        case "message":
            this.messageState = "moveUp";
            break;
    }
}
inputFocusOut(event:any) {

  if (this.rForm.get(event.target.id)!.value == null ||
      this.rForm.get(event.target.id)!.value != "") {
      return;
  }

  switch (event.target.id) {
      case "name":
          this.nameState = "moveDown";
          break;
      case "email":
          this.emailState = "moveDown";
          break;
      case "phone":
          this.phoneState = "moveDown";
          break;
        case "message":
          this.messageState = "moveDown";
          break;
  }


}
submitpost(post){
    if (this.rForm.valid) {

      console.log(post);
      this.email = post.email;
      this.name= post.name;
      this.phone = post.phone;
      this.message= post.message;
      this.formdata={
        "EmailFrom": this.email,
              "EmailTo": "matt@mfitconsultants.co.uk ",
              "IsHtml": false,
              "Subject": "shepherdplace notification",
              "Body": "Name: " + this.name + "\nEmail: " + this.email + "\nPhone: " + this.phone +  "\nMessage: " + this.message + "\n\n\nThis mail was sent from " + window.location.href
   
      }
     //  console.log(this.addApplicantTrader(this.formdata,"http://sendmail.mfitonline.co.uk/home/sendmail"));
       this.addApplicantTrader(this.formdata,"http://sendmail.mfitonline.co.uk/home/sendmail");

  }
  else {
    console.log("form is invalid");

    Object.keys(this.rForm.controls).forEach(key => {
        this.rForm.controls[key].markAsTouched();
        this.rForm.controls[key].markAsDirty();
    });
  }
  }
  addApplicantTrader(data, endpoint) {

    console.log(data);
    console.log(endpoint);
   return this.http.post(endpoint,  data)
   .map(response => {
    console.log(response); 
    return response;
     
 
   },
   error => {
     alert('invalid');
   }
   );
 }

  ngOnInit() {
  }

}
