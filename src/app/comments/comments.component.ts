import { Component, OnInit } from '@angular/core';
import { ServeService } from './../serve.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  commentData: any;
  pageNumber = 1;
  addTagForm: FormGroup;
  addTagSuccess = false;
  Status = false;
  clicktags = false;

  constructor(
      private serve: ServeService,
      private router: Router,
      private formBuilder: FormBuilder
    ) {
    this.getComments ();
   }

  ngOnInit() {
    this.serve.checkTokenExpiration();
    this.addTagFormFunc();
  }


  showComment(id) {
    this.serve.put('', '/Comment/approve/' + id)
    .subscribe(ret => {
      Swal(
        'Success!',
        'Updated tag succesfully.',
        'success'
      );
      this.getComments();
    });
  }
  getComments() {
    this.serve.get('/comment')
    .subscribe(ret => {
      this.commentData = ret;
  this.Status = true;
      },
      error => {
        console.log(error);
      });
  }
  deleteComment(id) {
    Swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.serve.delete('/comment/' + id)
    .subscribe(ret => {
      this.getComments();

    });
        Swal(
          'Deleted!',
          ' Comment has been deleted.',
          'success'
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal(
          'Cancelled',
          'Your file is safe ',
          'error'
        );
      }
    });
  }
  addTagFormFunc() {
    this.addTagForm = this.formBuilder.group({
      name: [''],
      id: [null]
    });
  }
  addTag() {
    const data = JSON.parse(JSON.stringify(this.addTagForm.value));
    this.serve.post(data, '/Tag')
    .subscribe(ret => {
      Swal(
        'Success!',
        'Submitted tag succesfully.',
        'success'
      );
     this.getComments();
     this.addTagForm.reset();
    });
  }
  Logout() {
    localStorage.clear();
     this.router.navigate(['/login']);
  }

}
