import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from '../globals';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  private status: boolean[];
  constructor(router: Router, private globals: Globals)
   {  this.status = globals.status;
  
    }
    public open(event, item) {
      for (let index = 0; index < 3; index++) {
        this.globals.status [index]=false;
      }
      this.globals.status [item]=true;
      console.log(this.globals.status);
    }
  

  ngOnInit() {
  }

}
