import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentSourcingComponent } from './investment-sourcing.component';

describe('InvestmentSourcingComponent', () => {
  let component: InvestmentSourcingComponent;
  let fixture: ComponentFixture<InvestmentSourcingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmentSourcingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentSourcingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
