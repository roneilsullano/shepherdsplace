import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { Component, OnInit , Injectable} from '@angular/core';
import{FormBuilder, FormGroup, Validators} from '@angular/forms';
import{HttpHeaders, HttpParams, HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';

export interface LoginData 
  {
    'name': any,
    'email':any,
    'phonenumber':any,
    'postcode':any,
    'subject':any,
    'message':any
  
  }

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
  rForm: FormGroup;
  post: any;
  formdata:any;
  name:string="";
  email:string="";
  phonenumber:string="";
  postcode:string="";
  subject:string="";
  message:string="";
  constructor(private fb:FormBuilder,  private http: HttpClient) {

    this.rForm = fb.group({
      'name':[null, Validators.required],
      'email':[null, Validators.required],
      'phonenumber':[null],
      'postcode':[null],
      'subject':[null, Validators.required],
      'message':[null, Validators.required],
    });

   }
   addPost(post){
     console.log(post);
    this.email = post.email;
    this.name= post.name;
    this.phonenumber = post.phonenumber;
    this.postcode= post.postcode;
    this.subject = post.subject;
    this.message= post.message;
    this.formdata={
      "EmailFrom": this.email,
            "EmailTo": "Contact@shepherdsplace.co.uk ",
            "IsHtml": false,
            "Subject": this.subject,
            "Body": "Name: " + this.name + "\nEmail: " + this.email + "\nPhone: " + this.phonenumber + "\nPost Code: " +this.postcode + "\nMessage: " + this.message + "\n\n\nThis mail was sent from " + window.location.href

    }
    console.log(this.addApplicantTrader(this.formdata,"http://sendmail.mfitonline.co.uk/home/sendmail"));
    // this.addApplicantTrader(this.formdata,"http://sendmail.mfitonline.co.uk/home/sendmail");
   }
   addApplicantTrader(data, endpoint) {
    return this.http.post(endpoint, data,{
      headers: new HttpHeaders().set('Content-type', 'application/json')})
    .map(response => {
      return response;
  
    },
    error => {
      alert('invalid');
    }
    );
  }


  ngOnInit() {
  }

}
