import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyDevelopmentComponent } from './property-development.component';

describe('PropertyDevelopmentComponent', () => {
  let component: PropertyDevelopmentComponent;
  let fixture: ComponentFixture<PropertyDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
